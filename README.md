LIMITED SQL PARSER
==================

[![CircleCI](https://circleci.com/bb/mparaiso/limited-sql-parser.svg?style=svg)](https://circleci.com/bb/mparaiso/limited-sql-parser)

[![GoDoc](https://godoc.org/bitbucket.org/mparaiso/limited-sql-parser?status.svg)](http://godoc.org/bitbucket.org/mparaiso/limited-sql-parser) [![Build Status](https://travis-ci.org/golang/gddo.svg?branch=master)](https://travis-ci.org/golang/gddo)


author: mparaiso <mparaiso@online.fr>

license: MIT

this is the implementation of the tutorial about writing 
a simple parser, available at this url: 

https://blog.gopheracademy.com/advent-2014/parsers-lexers/

https://www.youtube.com/watch?v=HxaD_trXwRE

### REQUIREMENTS

    install go : https://golang.org
    set up $GOPATH environement variable
    add $GOPATH/bin to $PATH

### INSTALLATION

    go get bitbucket.org/mparaiso/limited-sql-parser

### USAGE

    limited-sql-parser "SELECT * FROM products"


