// Package main parses an simple SQL SELECT statement
// as an input.
// It is the implementation of the following tutorial about
// writing a parser :
// https://blog.gopheracademy.com/advent-2014/parsers-lexers/
package main

import (
	"log"
	"os"
	"strings"

	"bitbucket.org/mparaiso/limited-sql-parser/parser"
)

func main() {
	args := reduce(os.Args[1:], func(result, element string) string {
		return result + " " + element
	})
	statement, err := parser.NewParser(strings.NewReader(args)).Parse()
	if err != nil {
		log.Fatal(err)
	}
	log.Print(statement)
}

func reduce(strings []string, reducer func(result string, element string) string) (output string) {
	for i := range strings {
		output = reducer(output, strings[i])
	}
	return
}
