package parser_test

import (
	"fmt"
	"strings"

	"bitbucket.org/mparaiso/limited-sql-parser/parser"
)

func ExampleParser() {
	reader := strings.NewReader("SELECT * FROM users")
	statement, err := parser.NewParser(reader).Parse()
	fmt.Println(err)
	fmt.Println(statement)
	// Output:
	// <nil>
	// TableName: users; Fields[ * ]
}

func ExampleParser_Second() {
	reader := strings.NewReader("SELECT * FROM users")
	statement, err := parser.NewParser(reader).Parse()
	fmt.Println(err)
	fmt.Println(statement)
	// Output:
	// <nil>
	// TableName: users; Fields[ * ]
}
