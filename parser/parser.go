package parser

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"strings"
)

// Token is a scanner token
type Token int

const (
	// Special tokens

	// ILLEGAL is an illegal token
	ILLEGAL Token = iota
	// EOF is an end of file token
	EOF
	// WS is a whitespace
	WS

	// Literals

	// IDENT is an identifier like a field or table name
	IDENT

	// Misc characters

	// ASTERISK *
	ASTERISK
	// COMMA is ,
	COMMA

	// Keywords

	// SELECT is keyword
	SELECT
	// FROM is a keyword
	FROM
)

// It’s also useful to define an “EOF” rune so that we can treat EOF like any other character:

var eof = rune(0)

func isWhiteSpace(ch rune) bool {
	return ch == ' ' || ch == '\t' || ch == '\n'
}

func isLetter(ch rune) bool {
	return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z')
}
func isDigit(ch rune) bool {
	return (ch >= '0' && ch <= '9')
}

// Scanner represents a lexical scanner.
type Scanner struct {
	r *bufio.Reader
}

// NewScanner creates a pointer to Scanner
func NewScanner(r io.Reader) *Scanner {
	return &Scanner{r: bufio.NewReader(r)}
}

// read reads the next rune from the bufferred reader.
// Returns the rune(0) if an error occurs (or io.EOF is returned).
func (s *Scanner) read() rune {
	ch, _, err := s.r.ReadRune()
	if err != nil {
		return eof
	}
	return ch
}

// unread places the previously read rune back on the reader.

func (s *Scanner) unread() { _ = s.r.UnreadRune() }

// Scan returns the next token and literal value.
func (s *Scanner) Scan() (token Token, literal string) {
	// If we see whitespace then consume all contiguous whitespace.
	// If we see a letter then consume as an ident or reserved word.
	ch := s.read()
	if isWhiteSpace(ch) {
		s.unread()
		return s.scanWhitespace()
	} else if isLetter(ch) {
		s.unread()
		return s.scanIdentifier()
	}
	// Otherwise read the individual character.
	switch ch {
	case eof:
		return EOF, ""
	case '*':
		return ASTERISK, string(ch)
	case ',':
		return COMMA, string(ch)
	}
	return ILLEGAL, string(ch)

}

// scanWhitespace consumes the current rune and all contiguous whitespace.
func (s *Scanner) scanWhitespace() (token Token, lit string) {
	var buf bytes.Buffer
	buf.WriteRune(s.read())
	// Read every subsequent whitespace character into the buffer.
	// Non-whitespace characters and EOF will cause the loop to exit.
	for {
		if ch := s.read(); ch == eof {
			break
		} else if !isWhiteSpace(ch) {
			s.unread()
			break
		} else {
			buf.WriteRune(ch)
		}
	}
	return WS, buf.String()
}

// scanIdent consumes the current rune and all contiguous ident runes.
func (s *Scanner) scanIdentifier() (token Token, literal string) {
	// Create a buffer and read the current character into it.

	var buffer bytes.Buffer
	buffer.WriteRune(s.read())
	// Read every subsequent ident character into the buffer.
	// Non-ident characters and EOF will cause the loop to exit.
	for {
		if ch := s.read(); ch == eof {
			break
		} else if !isLetter(ch) && !isDigit(ch) && ch != '_' {
			s.unread()
			break
		} else {
			_, _ = buffer.WriteRune(ch)
		}
	}
	// If the string matches a keyword then return that keyword.

	switch strings.ToUpper(buffer.String()) {
	case "SELECT":
		return SELECT, buffer.String()
	case "FROM":
		return FROM, buffer.String()
	}

	// Otherwise return as a regular identifier.
	return IDENT, buffer.String()
}

// Parser represents a parser
type Parser struct {
	scanner *Scanner
	buffer  struct {
		token   Token  // last read token
		literal string // last read literal
		n       int    // buffer size (max=1)
	}
}

// NewParser creates a pointer to parser
func NewParser(reader io.Reader) *Parser {
	return &Parser{scanner: NewScanner(reader)}
}

// scan returns the next token from the underlying scanner.
// If a token has been unscanned then read that instead.
func (parser *Parser) scan() (token Token, literal string) {
	// if we have a token on the buffer then return it
	if parser.buffer.n != 0 {
		parser.buffer.n = 0
		return parser.buffer.token, parser.buffer.literal
	}
	// Otherwise read the next token from the scanner
	token, literal = parser.scanner.Scan()
	// save it to the buffer in case we unscan later
	parser.buffer.token, parser.buffer.literal = token, literal
	return
}

func (parser *Parser) scanIgnoreWhitespace() (token Token, literal string) {
	token, literal = parser.scan()
	if token == WS {
		token, literal = parser.scan()
	}
	return
}

// unscan pushes the previously read token back onto the buffer
func (parser *Parser) unscan() { parser.buffer.n = 1 }

// Parse parses the reader and returns either a SelectStatement or an error
func (parser *Parser) Parse() (statement *SelectStatement, err error) {
	statement = new(SelectStatement)
	if token, literal := parser.scanIgnoreWhitespace(); token != SELECT {
		return nil, fmt.Errorf("found %q, expected SELECT", literal)
	}
	for {
		// read a field
		if token, literal := parser.scanIgnoreWhitespace(); token != IDENT && token != ASTERISK {
			return nil, fmt.Errorf("found %q expected field", literal)
		} else {
			statement.Fields = append(statement.Fields, literal)
		}
		// if the next token is not a comma then break the loop.
		if token, _ := parser.scanIgnoreWhitespace(); token != COMMA {
			parser.unscan()
			break
		}
	}
	if token, literal := parser.scanIgnoreWhitespace(); token != FROM {
		return nil, fmt.Errorf("found %q, expected FROM", literal)
	}
	if token, literal := parser.scanIgnoreWhitespace(); token != IDENT {
		return nil, fmt.Errorf("found %q ,expected table name", literal)
	} else {
		statement.TableName = literal
	}
	return
}

// SelectStatement represents a minimal SQL SELECT statement
type SelectStatement struct {
	Fields    []string
	TableName string
}

func (statement SelectStatement) String() (output string) {
	output += "TableName: " + statement.TableName + "; Fields[ "
	for i := range statement.Fields {
		if i != 0 {
			output += " , "
		}
		output += statement.Fields[i] + ", "
	}
	return output + " ]"

}
